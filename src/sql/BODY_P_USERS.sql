create or replace package body p_users
as
  
  procedure getUserById(
    in_id in integer,
    c_user out t_user_cursor)
  is
  begin
    open c_user for
      select usr_id, usr_username, usr_password
        from users
        where usr_id = in_id;
  end getUserById;
  
  procedure getUserByUsername(
    in_username in varchar2,
    c_user out t_user_cursor)
  is
  begin
    open c_user for
      select usr_id, usr_username, usr_password
        from users
        where usr_username = in_username;
  end getUserByUsername;
  
  -- zmienić dwie procedury na funkcje
  procedure createUser(
    in_username in varchar2,
    in_password in varchar2,
    out_result  out integer)
  is
    v_existing_username_count integer;
    ex_username_wrong_length exception;
    ex_password_wrong_length exception;
  begin
    select count(*)
      into v_existing_username_count
      from users
      where usr_username = in_username;
      
    if ( v_existing_username_count > 0 ) then
      out_result := -1;
      return;
    end if;
    
    if ( length(in_username) < 3 or length(in_username) > 50 ) then
      raise ex_username_wrong_length;
    end if;
    
    if ( length(in_password) < 3 or length(in_password) > 50 ) then
      raise ex_password_wrong_length;
    end if;
      
    insert into users
      (usr_username, usr_password)
    values
      (in_username, in_password);
      
    out_result := 0;
    
  exception
    when ex_username_wrong_length then
      out_result := -2;
      return;
    when ex_password_wrong_length then
      out_result := -3;
      return;
  end createUser;
    
  procedure validateUser(
    in_username     in varchar2,
    in_password     in varchar2,
    out_user_valid  out integer)
  is
    v_username varchar2(50);
    v_password varchar2(50);
    v_existing_username_count integer;
  begin
    select count(*)
      into v_existing_username_count
      from users
      where usr_username = in_username;
      
    select usr_password
      into v_password
      from users
      where usr_username = in_username;
      
    if ( v_existing_username_count = 0 
      or v_password ~= in_password ) then
      out_user_valid := 0;
      return;
    end if;
    
    out_user_valid := 1;
    
  exception
    when no_data_found then
      out_user_valid := 0;
      return;
    
  end;
  
end p_users;