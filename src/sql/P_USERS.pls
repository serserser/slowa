create or replace package p_users
as

  type t_number_array is table of integer;

  type t_user_record
  is
    record (
      usr_id       integer,
      usr_username varchar2(50),
      usr_password varchar2(50) );

  type t_user_cursor
  is ref cursor
    return t_user_record;

--    procedures
  procedure getUserById(
    in_id   in integer,
    c_user  out t_user_cursor);
    
  procedure getUserByUsername(
    in_username in varchar2,
    c_user out t_user_cursor);
    
  procedure createUser(
    in_username in varchar2,
    in_password in varchar2,
    out_result  out integer);  
    
  procedure validateUser(
    in_username     in varchar2,
    in_password     in varchar2,
    out_user_valid  out integer);
  
end p_users;