create or replace package p_tests 
as

  type t_test_record
  is
    record (
      tst_id integer,
      tst_name varchar2(50),
      tst_description varchar2(200),
      tst_first_col_desc varchar2(50),
      tst_second_col_desc varchar2(50)
    );
    
  type t_word_record
  is
    record (
      wrd_word_first  varchar2(50),
      wrd_word_second varchar2(50)
    );
      
    
  type t_test_cursor
  is ref cursor
    return t_test_record;
    
  type t_word_cursor
  is ref cursor
    return t_word_record;
    
    
    
  procedure getTestInfo(
    in_id in integer,
    c_test out t_test_cursor);
    
  procedure getUsersTests(
    in_usr_id in integer,
    c_test out t_test_cursor);
    
  procedure createTest(
    in_usr_id           in integer,
    in_name             in varchar2,
    in_description      in varchar2,
    in_first_col_desc   in varchar2,
    in_second_col_desc  in varchar2,
    out_result          out integer);

--  procedure addWordToTest(
--    in_usr_id           in integer,
--    in_tst_id           in varchar2,
--    in_word             in varchar2);

end p_tests;