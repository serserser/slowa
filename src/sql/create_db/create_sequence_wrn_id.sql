create sequence seq_wrn_id 
  start with 1
  increment by 1
  nomaxvalue
  nocache
  noorder;