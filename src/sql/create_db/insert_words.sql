insert into words (wrd_word, wrd_tst_id) values ('Laboratorium', 12);
insert into words (wrd_word, wrd_tst_id) values ('Lab', 12);
insert into words (wrd_word, wrd_tst_id) values ('Laboratory', 12);
insert into words (wrd_word, wrd_tst_id) values ('Robot przemysłowy', 12);
insert into words (wrd_word, wrd_tst_id) values ('Industrial robot', 12);


insert into words_relation (wrn_wrd_id_first, wrn_wrd_id_second)
  values (1, 2);
insert into words_relation (wrn_wrd_id_first, wrn_wrd_id_second)
  values (1, 3);
insert into words_relation (wrn_wrd_id_first, wrn_wrd_id_second)
  values (4, 5);


--insert into words (wrd_word, wrd_tst_id) values ('', );

select * from words;

select w1.wrd_word as pierwsza, w2.wrd_word as druga
  from words w1 join words_relation wr on w1.wrd_id = wr.wrn_wrd_id_first
                join words w2 on w2.wrd_id = wr.wrn_wrd_id_second
  where w1.wrd_tst_id = 12;
