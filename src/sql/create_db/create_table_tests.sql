create table tests
(
  tst_id integer not null,
  tst_usr_id integer not null,
  tst_name varchar2(50) not null,
  tst_description varchar2(200),
  tst_first_col_desc varchar2(50),
  tst_second_col_desc varchar2(50),
  tst_audit_create_user integer,
  tst_audit_create_date date,
  tst_audit_update_user integer,
  tst_audit_update_date date,
  tst_audit_delete_user integer,
  tst_audit_delete_date date,
  
  constraint pk_tst_id
    primary key (tst_id),  
    
  constraint fk_tst_usr_id 
    foreign key (tst_usr_id)
    references users (usr_id)
);