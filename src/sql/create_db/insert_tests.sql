insert into tests (tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc) 
  values (18, 'Kuchnia', 'Słowa związane z kuchnią i gotowaniem', 'Angielski', 'Polski');
insert into tests (tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc) 
  values (20, 'Studia', 'Słowa związane ze studiami', 'Rosyjski', 'Polski');
insert into tests (tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc) 
  values (18, 'Mieszkanie', 'Słowa rosyjskie - pomieszczenia w mieszkaniu', 'Polski', 'Rosyjski');
insert into tests (tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc) 
  values (18, 'Czasowniki', 'Odmiana rosyjskich czasowników', 'Rosyjski', 'Polski');
insert into tests (tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc) 
  values (21, 'Podróżowanie', 'Słowa związane z podróżowaniem', 'Angielski', 'Rosyjski');


select * from tests;

select * from users;

select * from t_tests
  where tst_usr_id in
  ( select usr_id
      from t_users
      where usr_username like '%jacek%');
      
select tst_id, tst_usr_id, tst_name, tst_description
  from t_users 
    join t_tests on tst_usr_id = usr_id
  where usr_username like '%jacek%';
  
select * from tests;

declare
  in_id integer := 11;
  c_out_cursor p_tests.t_test_cursor;
  rec p_tests.t_test_record;
begin
  p_tests.getTestInfo(in_id, c_out_cursor);
  
  loop
    fetch c_out_cursor into rec;
    exit when c_out_cursor%notfound;
    
    dbms_output.put_line('id: ' || rec.tst_id ||
                         '; name: ' || rec.tst_name ||
                         '; desc: ' || rec.tst_description ||
                         '; first col: ' || rec.tst_first_col_desc ||
                         '; second col: ' || rec.tst_second_col_desc);
  end loop;
end;
/

set serveroutput on;

declare
  in_usr_id     number := 18;
  c_out_cursor  p_tests.t_test_cursor;
  rec           p_tests.t_test_record;
begin
  p_tests.getUsersTests(in_usr_id, c_out_cursor);
  
  loop
    fetch c_out_cursor into rec;
    exit when c_out_cursor%notfound;
    
    dbms_output.put_line('id: ' || rec.tst_id ||
                         '; name: ' || rec.tst_name ||
                         '; desc: ' || rec.tst_description ||
                         '; first col: ' || rec.tst_first_col_desc ||
                         '; second col: ' || rec.tst_second_col_desc);
  end loop;
end;
/


declare 
  v_usr_id  number := 22;
  v_name    varchar2(50) := 'nazwa';
  v_description varchar2(200) := 'opis';
  v_first_col_desc varchar2(50) := 'pierwsza kolumna';
  v_second_col_desc varchar2(50) := 'druga kolumna';
  v_result integer;
begin
  p_tests.createTest(
    v_usr_id, 
    v_name, 
    v_description, 
    v_first_col_desc,
    v_second_col_desc,
    v_result);
  
  dbms_output.put_line('Uzyskano rezultat: ' || v_result);
end;
/

select * from users;

select * from words;

select * from words_relation;

select w1.wrd_word as first, w2.wrd_word as second
  from words w1 join words_relation wr on w1.wrd_id = wr.wrn_wrd_id_first
    join words w2 on w2.wrd_id = wr.wrn_wrd_id_second;
    
