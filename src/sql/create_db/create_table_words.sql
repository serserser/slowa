create table words
(
  wrd_id integer not null,
  wrd_word varchar2(200) not null,
  wrd_tst_id integer not null,
  wrd_audit_create_user integer,
  wrd_audit_create_date date,
  wrd_audit_update_user integer,
  wrd_audit_update_date date,
  wrd_audit_delete_user integer,
  wrd_audit_delete_date date,
  
  constraint pk_wrd_id
    primary key (wrd_id),
    
  constraint fk_wrd_tst_id
    foreign key (wrd_tst_id)
    references tests(tst_id)
);

--drop table words;