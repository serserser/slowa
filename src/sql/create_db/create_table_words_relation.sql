create table words_relation
(
  wrn_id integer not null,
  wrn_wrd_id_first integer not null,
  wrn_wrd_id_second integer not null,
  
  constraint pk_wrn_id
    primary key (wrn_id),
    
  constraint fk_wrn_wrd_id_first
    foreign key ( wrn_wrd_id_first )
    references words(wrd_id),
    
  constraint fk_wrn_wrd_id_second
    foreign key ( wrn_wrd_id_second )
    references words(wrd_id)
);

--drop table words_relation;