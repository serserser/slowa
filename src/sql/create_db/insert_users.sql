insert into users (usr_username, usr_password) values ('jacek666', 'jacek666');
insert into users (usr_username, usr_password) values ('kozak', 'haslo');
insert into users (usr_username, usr_password) values ('some_login', 'some_password');
insert into users (usr_username, usr_password) values ('another_log', 'another_pass');

update t_users 
  set usr_username = 'koza' ,
      usr_audit_update_user = 66
where usr_username = 'kozak';

select * from t_users;