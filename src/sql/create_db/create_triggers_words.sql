create or replace trigger trg_wrd_id
  before insert on words
  for each row
begin
  select seq_wrd_id.nextval
    into :new.wrd_id
    from dual;
end;
/

create or replace trigger trg_wrd_audit_create
  before insert on words
  for each row
begin
  select current_date
    into :new.wrd_audit_create_date
    from dual;
    
  if ( not updating(:new.wrd_audit_create_user) ) then
    select 1
      into :new.wrd_audit_create_user
      from dual;
  end if;
end;
/

create or replace trigger trg_wrd_audit_update
  before update on words
  for each row
begin
  select current_date
    into :new.wrd_audit_update_date
    from dual;
    
  if ( not updating(:new.wrd_audit_update_user) ) then
    select 1
      into :new.wrd_audit_update_user
      from dual;
  end if;
end;
/