create table users
(
  usr_id integer NOT NULL,
  usr_username varchar(50) NOT NULL,
  usr_password varchar(50) NOT NULL,
  usr_last_login date,
  usr_audit_create_user integer,
  usr_audit_create_date date,
  usr_audit_update_user integer,
  usr_audit_update_date date,
  usr_audit_delete_user integer,
  usr_audit_delete_date date,
  
  constraint pk_usr_Id
    primary key(usr_id)
);

--drop table users;