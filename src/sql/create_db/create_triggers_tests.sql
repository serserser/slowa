create or replace trigger trg_tst_id
  before insert on tests
  for each row
begin
  select seq_tst_id.nextval 
    into :new.tst_id
    from dual;
end;
/

create or replace trigger trg_tst_audit_create
  before insert on tests
  for each row
begin
  select current_date
    into :new.tst_audit_create_date
    from dual;

  if ( :new.tst_audit_create_user is null ) then
    select 1
      into :new.tst_audit_create_user
      from dual;
  end if;
end;
/

create or replace trigger trg_tst_audit_update
  before update on tests
  for each row
begin
  select current_date
    into :new.tst_audit_update_date
    from dual;

  if ( not updating('tst_audit_update_user') ) then
    select 1
      into :new.tst_audit_update_user
      from dual;
  end if;
end;
/