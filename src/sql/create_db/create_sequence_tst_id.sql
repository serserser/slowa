create sequence seq_tst_id 
  start with 1
  increment by 1
  nomaxvalue
  nocache
  noorder;