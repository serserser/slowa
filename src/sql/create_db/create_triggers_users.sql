create or replace trigger trg_usr_id
  before insert on users 
  for each row
begin
  select seq_usr_id.nextval
    into :new.usr_id
    from dual;
end;
/

create or replace trigger trg_usr_audit_create
  before insert on users
  for each row
begin
  select current_date
    into :new.usr_audit_create_date
    from dual;
  
  if ( :new.usr_audit_create_user is null ) then
    select 1
      into :new.usr_audit_create_user
      from dual;
  end if;
end;
/

create or replace trigger trg_usr_audit_update
  before update on users
  for each row
begin
  select current_date
    into :new.usr_audit_update_date
    from dual;
    
  if ( not updating('usr_audit_update_user') ) then
    select 1
      into :new.usr_audit_update_user
      from dual;
  end if;
end;
/