create tablespace nauka_perm datafile '/oracle/baza_nauka/nauka_perm_01.dat' size 10M autoextend on 
online;
create temporary tablespace nauka_temp tempfile '/oracle/baza_nauka/nauka_temp_01.dat' size 10M autoextend on;


create user nauka identified by nauka default tablespace nauka_perm temporary tablespace nauka_temp;

grant all privileges to nauka identified by nauka;