create trigger trg_wrn_id
  before insert on words_relation
  for each row
begin
  select seq_wrn_id.nextval
    into :new.wrn_id
    from dual;
end;