create or replace package body p_tests as
  
  procedure getTestInfo(
    in_id in integer,
    c_test out t_test_cursor)
  is
  begin
    open c_test for
      select tst_id, 
             tst_name, 
             tst_description, 
             tst_first_col_desc,
             tst_second_col_desc
        from tests
        where tst_id = in_id;
  end;
  
  procedure getUsersTests(
    in_usr_id in integer,
    c_test out t_test_cursor)
  is
  begin
    open c_test for
      select tst_id, 
             tst_name, 
             tst_description, 
             tst_first_col_desc,
             tst_second_col_desc
        from tests
        where tst_usr_id = in_usr_id;
  end;
  
  procedure createTest(
    in_usr_id           in integer,
    in_name             in varchar2,
    in_description      in varchar2,
    in_first_col_desc   in varchar2,
    in_second_col_desc  in varchar2,
    out_result          out integer)
  is
    v_num_usr_ids pls_integer;
    v_num_tests pls_integer;
  begin
    -- check if user exists
    select count(*)
      into v_num_usr_ids
      from users
      where usr_id = in_usr_id;
    
    if ( v_num_usr_ids = 0 ) then
      out_result := -1;
      return;
    end if;
    
    -- check if this test for this user exists
    select count(*)
      into v_num_tests
      from tests
      where 1=1 
        and tst_usr_id = in_usr_id
        and tst_name = in_name
        and 1=1;
        
    if ( v_num_tests >= 0 ) then
      out_result := -1;
      return;
    end if;
    
    -- insert new test
    insert into tests
      ( tst_usr_id, tst_name, tst_description, tst_first_col_desc, tst_second_col_desc)
    values ( in_usr_id, in_name, in_description, in_first_col_desc, in_second_col_desc);
    
  exception
    when no_data_found then
      -- not found the user
      out_result := -1;
      return;
  end;
  
--  procedure addWordToTest(
--    in_tst_id           in varchar2,
--    in_word             in varchar2)
--  is
--    v_num_existing_words      pls_integer;
--    invalid_foreign_key       exception;
--    pragma exception_init(invalid_foreign_key, -02291);
--  begin
--    -- check if this word already exists in this test
--    select count(*)
--      into v_num_existing_words
--      from words
--      where 1=1
--        and wrd_tst_id = in_tst_id
--        and wrd_word = in_word
--        and 1=1;
--      
--  end;
    
  
end p_tests;