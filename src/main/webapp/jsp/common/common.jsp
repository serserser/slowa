<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html style="height: 100%;">

<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html"/>
	<title>Słowa</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script type="text/css" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>"/>
</head>

<body style="height: 100%;">

<div class="container" style="height: 100%;">
	<div>
		<tiles:insert  attribute="header"/>
		<hr>
	</div>

	<tiles:insert name="leftMenu"/>

	<tiles:insert name="body"/>
</div>
</body>
</html>