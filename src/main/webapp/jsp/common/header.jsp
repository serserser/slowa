<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="row" id="header">
	<div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
		<h1>Program Słowa</h1>
	</div>
	<c:choose>
		<c:when test="${sessionScope.authenticated }">
			<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" id="userInfo">
				<form action="logout.action">
					Logged in as ${username}
					<br/>
					<input type="submit" class="btn btn-default" value="Log out"/>
				</form>
			</div>
		</c:when>
		<c:otherwise>
			<div class="col-lg-offset-3 col-md-offset-4 col-sm-offset-5 col-xs-offset-12">
				&nbsp;
			</div>
		</c:otherwise>
	</c:choose>
</div>
<div>
	<ul class="nav nav-pills navbar">
		<li role="presentation"><a href="login_form.action">Zaloguj</a></li>
		<li role="presentation"><a href="registration_form.action">Zarejestruj się</a></li>
		<li role="presentation"><a href="private.action">Część prywatna</a></li>
		<li role="presentation"><a href="public.action">Część publiczna</a></li>
		<li role="presentation"><a href="homePage.action">Strona główna</a></li>
	</ul>
</div>
