<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-sm-9 text-center" id="content">
	<div class="col-sm-offset-3 col-sm-6 col-sm-offset-3" id="dialog">
		<h3>Zaloguj się</h3>
		<form action="login.action" method="post">
			<input type="text" name="login" class="form-control" placeholder="Login" required autofocus>
			<input type="password" name="password" class="form-control" placeholder="Password" required autofocus>
			<button class="btn btn-primary btn-block" type="submit">Zaloguj się</button>
		</form>
	</div>
</div>