<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-sm-9 text-center" id="content">
	<h3>This is your private part</h3>
	This part of the app should be used to remember and present to you as a list, anything you write into the textbox
	below
	<form action="save_data.action" method="post">
		<input type="text" name="currentData">
		<input type="submit" value="Save">
	</form>

	<h4>Your data</h4>
	<ol>
		<c:forEach items="${sessionScope.userData}" var="dataEntry">
			<li>${dataEntry}</li>
		</c:forEach>
	</ol>

	<form action="homePage.action">
		<input type="submit" value="Back to home page">
	</form>
</div>
