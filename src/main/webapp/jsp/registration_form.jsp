<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-sm-9 text-center" id="content">
	<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-2 col-lg-offset-3 col-xs-10 col-sm-10 col-md-8 col-lg-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3 col-lg-offset-3"
		 id="dialog">
		<h3>Podaj dane potrzebne do rejestracji</h3>
		<form action="register.action" method="post">
			<div class="form-group">
				<label for="login" class="col-sm-3 control-label">Login</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="login" name="login" autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-3 control-label">Password</label>
				<div class="col-sm-9">
					<input type="password" class="form-control" id="password" name="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<div class="checkbox">
						<label>
							<input type="checkbox" value="">Wyrażam zgodę na blablabla</label>
					</div>
				</div>
			</div>
			<input type="submit" value="Zarejestruj">
		</form>
	</div>
</div>
