<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-sm-9 text-center" id="content">
	<h2>You are not authorized to view this content</h2>
	<form action="homePage.action">
		<input type="submit" value="Go back to home page">
	</form>
	<form action="login_form.action">
		<input type="submit" value="Go to login page">
	</form>
</div>
