<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="row" id="header">
	<div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
		<h1>Program Słowa</h1>
	</div>
	<c:choose>
		<c:when test="${sessionScope.authenticated }">
			<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12" id="userInfo">
				<form action="logout.action">
					Logged in as ${username}
					<br/>
					<input type="submit" class="btn btn-default" value="Log out"/>
				</form>
			</div>
		</c:when>
		<c:otherwise>
			<div class="col-lg-offset-3 col-md-offset-4 col-sm-offset-5 col-xs-offset-12">
				&nbsp;
			</div>
		</c:otherwise>
	</c:choose>
</div>