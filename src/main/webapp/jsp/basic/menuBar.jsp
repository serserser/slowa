<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div>
	<ul class="nav nav-pills navbar">
		<li role="presentation"><a href="login_form.action">Zaloguj</a></li>
		<li role="presentation"><a href="registration_form.action">Zarejestruj się</a></li>
		<li role="presentation"><a href="private.action">Część prywatna</a></li>
		<li role="presentation"><a href="public.action">Część publiczna</a></li>
		<li role="presentation"><a href="homePage.action">Strona główna</a></li>
	</ul>
</div>

<hr>
