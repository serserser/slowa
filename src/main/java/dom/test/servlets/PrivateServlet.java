package dom.test.servlets;

import dom.test.logic.DummyUserBussinessLogic;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(
		value = "/save_data.action"
)
public class PrivateServlet extends HttpServlet
{
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String currentData = request.getParameter("currentData");
		DummyUserBussinessLogic logic = DummyUserBussinessLogic.getInstance();

		String username = (String) request.getSession().getAttribute("username");
		logic.addUserDataItem(username, currentData);

		String redirectURL = "/jsp/private.jsp";
		RequestDispatcher dispatcher = request.getRequestDispatcher(redirectURL);
		dispatcher.forward(request, response);
	}
}
