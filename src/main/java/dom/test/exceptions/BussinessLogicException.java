package dom.test.exceptions;

public class BussinessLogicException extends RuntimeException
{
	public BussinessLogicException(String message)
	{
		super(message);
	}
}
