package dom.test.logic;

import dom.test.domain.User;

public class UserManager
{
	private UserManager instance;

	public UserManager getInstance()
	{
		if ( instance == null )
		{
			instance = new UserManager();
		}
		return instance;
	}
}
