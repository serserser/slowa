package dom.test.logic;

import dom.test.domain.User;
import dom.test.exceptions.BussinessLogicException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyUserBussinessLogic
{

	private static DummyUserBussinessLogic instance = null;

	private Map<String, String> users;

	private Map<String, List<String>> userData;

	private DummyUserBussinessLogic()
	{
		users = new HashMap<String, String>();
		userData = new HashMap<String, List<String>>();
	}

	public static DummyUserBussinessLogic getInstance()
	{
		if ( instance == null )
		{
			instance = new DummyUserBussinessLogic();
		}
		return instance;
	}

	public void createUser(User user)
	{
		if ( user == null )
			throw new BussinessLogicException("The user should not be null");

		if ( users.containsKey(user.getUsername()) )
			throw new BussinessLogicException("This user already exists in database");

		addUserToDB(user);
	}

	public boolean isUserValid(User user)
	{
		if ( users.containsKey(user.getUsername())
				&& users.get(user.getUsername()).equals(user.getPassword()) )
			return true;
		else
			return false;
	}

	public boolean isUserLoginValid(String userLogin)
	{
		if ( users.containsKey(userLogin) )
			return true;
		else
			return false;
	}

	public List<String> getUserData(User user)
	{
		if ( ! isUserValid(user) )
			throw new BussinessLogicException("The requested user does not exist in database");

		return userData.get(user.getUsername());
	}

	public void addUserDataItem(String username, String userDataItem)
	{
		if ( username == null || "".equals(username) )
			throw new BussinessLogicException("The requested user should not be null or empty");
		if ( userDataItem == null || "".equals(userDataItem) )
			throw new BussinessLogicException("Cannot save empty data");
		if ( ! isUserLoginValid(username) )
			throw new BussinessLogicException("The requested user does not exist in database");

		List<String> data = userData.get(username);
		data.add(userDataItem);
	}


	private void addUserToDB(User user)
	{
		users.put(user.getUsername(), user.getPassword());
		userData.put(user.getUsername(), new ArrayList<String>());
	}
}
