package dom.test.logic.persistenceLayer.utilities.exceptions;

public class DBException extends RuntimeException
{
	public DBException(String message)
	{
		super(message);
	}
}
