package dom.test.logic.persistenceLayer.utilities.exceptions;

public class MultipleResultsException extends RuntimeException
{
	public MultipleResultsException(String message)
	{
		super(message);
	}
}
