package dom.test.logic.persistenceLayer.implementations;


import dom.test.domain.User;
import dom.test.logic.persistenceLayer.DAOs.UserDataLogic;
import dom.test.logic.persistenceLayer.utilities.exceptions.DBException;
import dom.test.logic.persistenceLayer.utilities.exceptions.MultipleResultsException;
import oracle.jdbc.OracleTypes;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public class UserDataLogicImpl implements UserDataLogic
{
	private static UserDataLogicImpl instance;

	DataSource dataSource;

	private static final String GET_USER = "{call p_users.getUser(?, ?)}";
	private static final String CREATE_USER = "{call p_users.createUser(?, ?, ?)}";
	private static final String VALIDATE_USER = "{call p_users.validateUser(?, ?, ?)}";

	private UserDataLogicImpl()
	{
		try
		{
			Context context = new InitialContext();
			Context envContext = (Context) context.lookup("java:comp/env");
			dataSource = (DataSource) envContext.lookup("jdbc/slowa");
		} catch ( NamingException exc )
		{
			System.err.println("Failed to connect to DB due to: " + exc.getMessage());
			exc.printStackTrace();
		}
	}

	public static UserDataLogicImpl getInstance()
	{
		if ( instance == null )
		{
			instance = new UserDataLogicImpl();
		}
		return instance;
	}

	public void createUser(User user)
	{
		int result = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			CallableStatement statement = connection.prepareCall(CREATE_USER);

			int counter = 1;
			statement.setString(counter++, user.getUsername());
			statement.setString(counter++, user.getPassword());
			int errorCodeArgNumber = counter;
			statement.registerOutParameter(counter++, OracleTypes.INTEGER);

			statement.execute();

			result = statement.getInt(errorCodeArgNumber);
		} catch ( SQLException exc )
		{
			String message = "Caught an SQLException: " + exc.getMessage();
			System.err.println(message);
			exc.printStackTrace();
			throw new DBException(message);
		}

		if ( result == -1 )
		{
			throw new DBException("User with this username already exists in DB");
		} else if ( result == -2 )
		{
			throw new DBException("The username length is invalid");
		} else if ( result == -3 )
		{
			throw new DBException("The password length is invalid");
		}
	}

	public User getUser(long userId)
	{
		User user;

		try
		{
			Connection connection = dataSource.getConnection();
			CallableStatement statement = connection.prepareCall(GET_USER);
			statement.setLong(1, userId);
			statement.registerOutParameter(2, OracleTypes.CURSOR);

			statement.executeQuery();
			ResultSet set = (ResultSet) statement.getObject(2);

			set.next();

			int id;
			String username;
			String password;

			id = set.getInt("USR_ID");
			username = set.getString("USR_USERNAME");
			password = set.getString("USR_PASSWORD");

			System.out.println("Retrieved record:");
			System.out.println("\tid: " + id);
			System.out.println("\tusername: " + username);
			System.out.println("\tpassword: " + password);

			user = new User(id, username, password);

			if ( set.next() )
			{
				throw new MultipleResultsException("There were multiple results, when getting user by user id");
			}

			connection.close();

		} catch ( SQLException exc )
		{
			String message = "Caught an SQLException: " + exc.getMessage();
			exc.printStackTrace();
			System.err.println(message);
			throw new DBException(message);
		}

		return user;
	}

	public boolean validateUser(User user)
	{
		int result = 1;

		try
		{
			Connection connection = dataSource.getConnection();
			CallableStatement statement = connection.prepareCall(VALIDATE_USER);

			int counter = 1;
			statement.setString(counter++, user.getUsername());
			statement.setString(counter++, user.getPassword());
			int errorCodeArgumentNumber = counter;
			statement.registerOutParameter(counter++, OracleTypes.INTEGER);

			statement.execute();
			result = statement.getInt(errorCodeArgumentNumber);
		} catch ( SQLException exc )
		{
			String message = "Caught an SQLException: " + exc.getMessage();
			exc.printStackTrace();
			System.err.println(message);
			throw new DBException(message);
		}

		if ( result == 1 )
			return true;
		else
			return false;
	}
}
