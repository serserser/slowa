package dom.test.logic.persistenceLayer.DAOs;

import dom.test.domain.User;

import java.util.List;

public interface UserDataLogic
{
	User getUser(long userId);
	void createUser(User user);
	boolean validateUser(User user);
}
