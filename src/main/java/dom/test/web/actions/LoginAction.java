package dom.test.web.actions;

import dom.test.domain.User;
import dom.test.logic.persistenceLayer.DAOs.UserDataLogic;
import dom.test.logic.persistenceLayer.implementations.UserDataLogicImpl;
import dom.test.web.forms.LoginForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginAction extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
								 HttpServletRequest request, HttpServletResponse response)
	{
		LoginForm loginForm = (LoginForm) form;

		String login = loginForm.getLogin();
		String password = loginForm.getPassword();
		User user = new User(login, password);

		UserDataLogic logic = UserDataLogicImpl.getInstance();
		boolean isUserValid = logic.validateUser(user);

		if ( isUserValid )
		{
			request.getSession().setAttribute("authenticated", true);
			request.getSession().setAttribute("username", user.getUsername());
			request.setAttribute("username", user.getUsername());

			return mapping.findForward("success");
		} else
		{
			request.getSession().setAttribute("authenticated", "false");
			return mapping.findForward("failure");
		}

	}
}
