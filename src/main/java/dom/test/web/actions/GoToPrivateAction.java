package dom.test.web.actions;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoToPrivateAction extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
								 HttpServletRequest request, HttpServletResponse response)
	{
		Boolean isUserAuthenticated = (Boolean) request.getSession().getAttribute("authenticated");

		if ( isUserAuthenticated != null &&
				isUserAuthenticated.booleanValue() )
		{
			return mapping.findForward("success");
		} else
		{
			return mapping.findForward("failure");
		}
	}
}
