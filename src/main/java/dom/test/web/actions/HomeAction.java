package dom.test.web.actions;

import dom.test.web.forms.HomeForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeAction extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
								 HttpServletRequest request, HttpServletResponse response)
	{
		HomeForm homeForm = (HomeForm) form;
		homeForm.setMessage("Everything went correctly with struts");

		return mapping.findForward("success");
	}
}
