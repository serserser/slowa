package dom.test.web.actions;

import dom.test.domain.User;
import dom.test.logic.persistenceLayer.DAOs.UserDataLogic;
import dom.test.logic.persistenceLayer.implementations.UserDataLogicImpl;
import dom.test.logic.persistenceLayer.utilities.exceptions.DBException;
import dom.test.web.forms.RegisterForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterAction extends Action
{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
								 HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			RegisterForm registerForm = (RegisterForm) form;

			String login = registerForm.getLogin();
			String password = registerForm.getPassword();
			User user = new User(login, password);

			UserDataLogic dataLogic = UserDataLogicImpl.getInstance();
			dataLogic.createUser(user);
		} catch ( DBException exc )
		{
			String message = "Failed to register user due to: " + exc.getMessage();
			System.out.println(message);
			return mapping.findForward("failure");
		}

		return mapping.findForward("success");
	}
}
