package dom.test.web.forms;

import org.apache.struts.action.ActionForm;

public class RegisterForm extends ActionForm
{
	String login;
	String password;

	String message;

	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}
